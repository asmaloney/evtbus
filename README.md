# evtbus

Basic Event Bus library for Go

> Note that this library uses generics and requires Go 1.18 or higher.

## Basic Usage

```go
import "gitlab.com/snocorp/evtbus"

type update struct {
    oldValue int
    newValue int
}

func main() {
    handle := evtbus.Subscribe("updates", func(u update) {
        fmt.Printf("old: %v, new %v", u.oldValue, u.newValue)
    })

    evtbus.Publish("updates", update{1, 2})

    evtbus.Unsubscribe(handle)

    evtbus.Publish("updates", update{2, 3}) // no output
}

// Output:
// old: 1, new 2
```

## Asynchronous Usage

```go
import "gitlab.com/snocorp/evtbus"

type update struct {
    oldValue int
    newValue int
}

func main() {
    evtbus.SubscribeAsync("updates", func(u update) {
        fmt.Printf("old: %v, new %v", u.oldValue, u.newValue)
    })

    handle := evtbus.Publish("updates", update{1, 2})

    // wait for all subscribers to finish
    evtbus.Wait(handle)
}

// Output:
// old: 1, new 2
```



