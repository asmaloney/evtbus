package main

import (
	"sync"
	"sync/atomic"
)

// Handler is a function that accepts data from the event bus that was published to the bus.
type Handler[T any] func(data T)

// UnsubsribeHandle is a function that allows the subscriber to stop subscribing to published data.
type UnsubscribeHandle = func()

// EventHandle is an interface that allows a publisher to wait until all asynchronous subscribers are done.
type EventHandle interface {
	Wait()
}

// subscriber is a structure that holds the handlers as well as an index that allows unsubscribing from the channel.
type subscriber[T any] struct {
	// handler will be invoked whenever data is published to the channel
	handler Handler[T]

	// index is an identifier to allow unsubscribing from the channel, must be unique per channel
	index uint32

	async bool
}

// channel is a structure that allows subscribers to listen to published data
type channel[T any] struct {
	// topic is the name of the channel
	topic string

	// subscribers is the current list of listeners to the channel
	subscribers sync.Map // map[uint32]subscriber[T]

	// counter is an incrementing integer is set on each subscriber to allow unsubscribing from the channel
	counter uint32
}

// channelMap stores a map of topics to channels.
var channelMap = sync.Map{}

/* GetChannel returns a typed channel from the channel map */
func GetChannel[T any](topic string) *channel[T] {
	c := &channel[T]{
		topic: topic,
		subscribers: sync.Map{},
		counter: 0,
	}
	x, ok := channelMap.LoadOrStore(topic, c)
	if ok {
		c = x.(*channel[T])
	}
	return c
}

// Subscribe adds a handler to the subscribers of a channel. The handler will be invoked whenever data is published to 
// the channel.
func Subscribe[T any](topic string, handler Handler[T]) UnsubscribeHandle {
	c := GetChannel[T](topic)
	return c.Subscribe(handler)
}

// SubscribeAsync adds an asynchronous handler to the channel which can be run in parallel with other subscribers.
func SubscribeAsync[T any](topic string, handler Handler[T]) UnsubscribeHandle {
	c := GetChannel[T](topic)
	return c.SubscribeAsync(handler)
}

// SubscribeOnce adds a handler to the subscribers of a channel. The handler will only be invoked once and then will
// be unsubscribed.
func SubscribeOnce[T any](topic string, handler Handler[T]) UnsubscribeHandle {
	c := GetChannel[T](topic)
	return c.SubscribeOnce(handler)
}

// SubscribeOnceAsync adds an async handler to the subscribers of a channel. The handler will only be invoked once and
// then will be unsubscribed.
func SubscribeOnceAsync[T any](topic string, handler Handler[T]) UnsubscribeHandle {
	c := GetChannel[T](topic)
	return c.SubscribeOnceAsync(handler)
}

// Publish sends data to the channel will will be send to all subscribers to the channel.
func Publish[T any](topic string, data T) EventHandle {
	c := GetChannel[T](topic)
	return c.Publish(data)
}

// Unsubscribe uses the given handle to unsubscribe from the channel
func Unsubscribe(handle UnsubscribeHandle) {
	handle()
}

// Wait blocks until the given event handle has no more subscribers in progress.
func Wait(handle EventHandle) {
	handle.Wait()
}

// Subscribe adds a handler to the subscribers of a channel. The handler will be invoked whenever data is published to 
// the channel.
func (c *channel[T]) Subscribe(handler Handler[T]) UnsubscribeHandle {
	index := c.subscribe(handler, false)

	// return a handle that allows the caller to unsubscribe
	return c.unsubscriber(index)
}

// SubscribeAsync adds an asynchronous handler to the channel which can be run in parallel with other subscribers.
func (c *channel[T]) SubscribeAsync(handler Handler[T]) UnsubscribeHandle {
	index := c.subscribe(handler, true)

	// return a handle that allows the caller to unsubscribe
	return c.unsubscriber(index)
}

// SubscribeOnce adds a handler to the subscribers of a channel. The handler will only be invoked once and then will 
// be unsubscribed.
func (c *channel[T]) SubscribeOnce(handler Handler[T]) UnsubscribeHandle {
	return c.subscribeOnce(handler, false)
}

// SubscribeOnceAsync adds an async handler to the subscribers of a channel. The handler will only be invoked once and then will 
// be unsubscribed.
func (c *channel[T]) SubscribeOnceAsync(handler Handler[T]) UnsubscribeHandle {
	return c.subscribeOnce(handler, true)
}

// subscribeOnce adds a handler to the subscribers of a channel. The handler will only be invoked once and then will 
// be unsubscribed.
func (c *channel[T]) subscribeOnce(handler Handler[T], async bool) UnsubscribeHandle {
	index := atomic.LoadUint32(&c.counter)
	unsub := c.unsubscriber(index)
	onceHandler := func(data T) {
		handler(data)
		unsub()
	}
	c.subscribe(onceHandler, async)

	return unsub
}

func (c *channel[T]) subscribe(handler Handler[T], async bool) uint32 {
	index := atomic.LoadUint32(&c.counter)
	s := subscriber[T]{
		handler: handler,
		index: index,
		async: async,
	}
	c.subscribers.Store(index, s)
	atomic.AddUint32(&c.counter, 1)

	return index
}

// unsubscriber returns a function that removes the subscriber from the channel
func (c *channel[T]) unsubscriber(index uint32) func() {
	return func() {
		c.subscribers.Delete(index)
	}
}

// Publish sends data to the channel which will be sent to all subscribers to the channel.
// Returns an event handle that can be used to wait for all asynchronous subscribers to be done. 
func (c *channel[T]) Publish(data T) EventHandle {
	var wg sync.WaitGroup
	c.subscribers.Range(func(key, value any) bool {
		s := value.(subscriber[T])
		if s.async {
			wg.Add(1)
			go func(data T) {
				defer wg.Done()
				s.handler(data)
			}(data)
		} else {
			s.handler(data)
		}
		return true
	})

	return &wg
}